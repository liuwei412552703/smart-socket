package net.vinote.smart.socket.extension.cluster;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import net.vinote.smart.socket.exception.CacheFullException;
import net.vinote.smart.socket.extension.cluster.balance.LoadBalancing;
import net.vinote.smart.socket.extension.timer.QuickTimerTask;
import net.vinote.smart.socket.lang.QuicklyConfig;
import net.vinote.smart.socket.protocol.DataEntry;
import net.vinote.smart.socket.service.process.AbstractProtocolDataProcessor;
import net.vinote.smart.socket.transport.TransportSession;
import net.vinote.smart.socket.transport.nio.NioQuickClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 集群服务管理器,被绑定了当前处理器的客户端消息将由该处理器发送至集群服务器
 *
 * @author Seer
 *
 */
public class Client2ClusterMessageProcessor extends AbstractProtocolDataProcessor {
	private Logger logger = LoggerFactory.getLogger(Client2ClusterMessageProcessor.class);

	public static Client2ClusterMessageProcessor getInstance() {
		if (instance == null) {
			synchronized (Client2ClusterMessageProcessor.class) {
				if (instance == null) {
					instance = new Client2ClusterMessageProcessor();
				}
			}
		}
		return instance;
	}

	private static Client2ClusterMessageProcessor instance;
	private ArrayBlockingQueue<ProcessUnit> msgQueue;
	private ClusterServiceProcessThread processThread;
	private ConcurrentMap<String, ProcessUnit> clientTransSessionMap;

	/**
	 * 客户端链接监控
	 */
	private QuickTimerTask sessionMonitorTask;

	private Client2ClusterMessageProcessor() {
	}

	/**
	 * 创建集群服务配置对象
	 *
	 * @param baseConfig
	 * @param url
	 * @return
	 */
	private QuicklyConfig createClusterQuickConfig(final QuicklyConfig baseConfig, final QuickURL url) {
		QuicklyConfig config = new QuicklyConfig(baseConfig.isServer());
		config.setCacheSize(baseConfig.getCacheSize());
		config.setProtocolFactory(baseConfig.getProtocolFactory());
		config.setAutoRecover(true);
		config.setFilters(baseConfig.getFilters());
		config.setQueueOverflowStrategy(baseConfig.getQueueOverflowStrategy());
		config.setLocalIp(baseConfig.getLocalIp());
		config.setHost(url.getIp());
		config.setPort(url.getPort());
		config.setProcessor(new Cluster2ClientMessageProcessor());
		return config;
	}

	public ClusterMessageEntry generateClusterMessage(DataEntry data) {
		throw new UnsupportedOperationException(this.getClass().getSimpleName() + " is unsupport current operation!");
	}

	/**
	 * 获取客户端链接对象
	 *
	 * @param clientUniqueNo
	 * @return
	 */
	public TransportSession getClientTransportSession(String clientUniqueNo) {
		ProcessUnit unit = clientTransSessionMap.get(clientUniqueNo);
		return unit == null ? null : unit.clientSession;
	}

	@Override
	public void init(final QuicklyConfig baseConfig) throws Exception {
		super.init(baseConfig);
		String[] clusterUrls = baseConfig.getClusterUrl();
		if (clusterUrls != null && clusterUrls.length > 0) {
			List<QuickURL> urlList = new ArrayList<QuickURL>(clusterUrls.length);
			for (String url : baseConfig.getClusterUrl()) {
				urlList.add(new QuickURL(url.split(":")[0], Integer.valueOf(url.split(":")[1])));
			}

			if (!urlList.isEmpty()) {
				for (final QuickURL url : urlList) {
					// 构造集群配置
					QuicklyConfig config = createClusterQuickConfig(baseConfig, url);

					NioQuickClient client = new NioQuickClient(config);
					client.start();
					logger.info("Connect to Cluster Server[ip:" + url.getIp() + " ,port:" + url.getPort() + "]");
					getQuicklyConfig().getLoadBalancing().registServer(client);
				}
			}
		}
		clientTransSessionMap = new ConcurrentHashMap<String, ProcessUnit>();
		msgQueue = new ArrayBlockingQueue<ProcessUnit>(baseConfig.getCacheSize());
		processThread = new ClusterServiceProcessThread("Quickly-Cluster-Process-Thread", this, msgQueue);
		processThread.start();

		// 定时扫描客户端链路有效性
		sessionMonitorTask = new SessionMonitorTask();
	}

	/**
	 * 定时扫描客户端链路有效性
	 *
	 * @author Seer
	 * @version Client2ClusterMessageProcessor.java, v 0.1 2015年8月25日 下午4:12:31
	 *          Seer Exp.
	 */
	private class SessionMonitorTask extends QuickTimerTask {

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * net.vinote.smart.socket.extension.timer.QuickTimerTask#getPeriod()
		 */
		@Override
		protected long getPeriod() {
			return TimeUnit.SECONDS.toMillis(30);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			for (String key : clientTransSessionMap.keySet()) {
				ProcessUnit unit = clientTransSessionMap.get(key);
				if (!unit.clientSession.isValid()) {
					unit.clientSession.close();
					clientTransSessionMap.remove(key);
					logger.info("remove invalid client[IP:" + unit.clientSession.getRemoteAddr() + " ,Port:"
						+ unit.clientSession.getRemotePort() + "]");
				}
			}
		}

	}

	/**
	 * 仅负责消息分发,集群服务器的响应消息由各连接器处理
	 */

	public <T> void process(T processUnit) {
		ProcessUnit unit = (ProcessUnit) processUnit;
		// 指定集群服务器
		try {
			unit.clusterSession.write((DataEntry) unit.msg);
		} catch (IOException e) {
			logger.warn(e.getMessage(), e);
		} catch (CacheFullException e) {
			logger.warn(e.getMessage(), e);
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}
	}

	/**
	 *
	 * 接受客户端消息以便转发至集群服务器
	 **/
	public boolean receive(TransportSession session, DataEntry msg) {
		if (!clientTransSessionMap.containsKey(session.getSessionID())) {
			clientTransSessionMap.put(session.getSessionID(), new ProcessUnit(session, null, null));
		}
		TransportSession clusterSession = clientTransSessionMap.get(session.getSessionID()).clusterSession;
		// 分配集群服务器
		if (clusterSession == null) {
			clusterSession = getQuicklyConfig().getLoadBalancing().balancing(session);
			clientTransSessionMap.get(session.getSessionID()).clusterSession = clusterSession;
		}
		ClusterMessageEntry clusterReq = getQuicklyConfig().getProcessor().generateClusterMessage(msg);// 封装集群消息
		clusterReq.setUniqueNo(session.getSessionID());
		return msgQueue.offer(new ProcessUnit(session, clusterSession, clusterReq));
	}

	public void shutdown() {
		if (processThread != null) {
			processThread.shutdown();
		}
		if (sessionMonitorTask != null) {
			sessionMonitorTask.cancel();
		}
		if (getQuicklyConfig() != null) {
			LoadBalancing load = getQuicklyConfig().getLoadBalancing();
			if (load != null) {
				load.shutdown();
			}
		}
	}
}
