package net.vinote.smart.socket.transport.enums;

/**
 * 传输层会话状态
 * 
 * @author Seer
 * @version SessionStatus.java, v 0.1 2015年3月21日 下午4:34:26 Seer Exp.
 */
public enum SessionStatusEnum {
	CLOSED, CLOSING, ENABLED;
}
