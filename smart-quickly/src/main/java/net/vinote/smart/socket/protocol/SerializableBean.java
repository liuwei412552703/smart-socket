package net.vinote.smart.socket.protocol;

/**
 * 序列化对象
 * 
 * @author Seer
 * @version DataEntry.java, v 0.1 2015年8月31日 下午7:23:57 Seer Exp.
 */
public class SerializableBean {

	private Object bean;

	public Object getBean() {
		return bean;
	}

	public void setBean(Object bean) {
		this.bean = bean;
	}

}
